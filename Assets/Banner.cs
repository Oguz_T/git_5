﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using UnityEngine.Advertisements;

public class Banner: MonoBehaviour
{
	
	static InterstitialAd interstitial;
	static BannerView bannerView;
	static string interstitialID;
	static string bannerViewID;
	static AdRequest adRequestObject;
	static bool isInterstitialReady = false;
	int bannerController, instertialAdController;
	//private static GoogleAnalyticsScene googleAnalyticsScene;

	void Start ()
	{
		Advertisement.Initialize ("1006508");
		bannerController= 0;
		instertialAdController = 0;
		//android =>
		
		interstitialID = "ca-app-pub-2280260698997458/3686666725";
		bannerViewID = "ca-app-pub-2280260698997458/2209933525";

		createInstertialAd ();

		//AdSize adSize = new AdSize(320, 50);
		bannerView = new BannerView (bannerViewID, AdSize.SmartBanner, AdPosition.Top);
		adRequestObject = new AdRequest.Builder ().Build ();

		//googleAnalyticsScene = gameObject.GetComponent<GoogleAnalyticsScene> ();

	}
	public void showAndHideBanner(){

		if (bannerController == 0) {
			showBanner ();
			bannerController = 1;
		} else {
			hideSmartBanner();
			bannerController = 0;
		}
	
	} 
	public void showInstertailAd(){
			
			showInterstitialAd (1);
		 
	}


	void OnLevelWasLoaded (int level)
	{
		createInstertialAd ();
	}

	void OnApplicationQuit ()
	{
		//googleAnalyticsScene.LogLeftApplication ("Normal zamanda çıktı");
	}

	public static void createInstertialAd ()
	{
		if (!isInterstitialReady) {
			interstitial = new InterstitialAd (interstitialID);
			interstitial.AdLoaded += HandleAdLoaded;
			interstitial.AdClosed += HandleAdClosed;
			interstitial.AdLeftApplication += HandleAdLeftApplication;
			AdRequest adRequest = new AdRequest.Builder ().Build ();
			interstitial.LoadAd (adRequest);
		}
	}

	public static void HandleAdLoaded (object sender, System.EventArgs args)
	{
		//print ("ALOOOOOOOOOOO....... HandleAdLoaded event received.");
		isInterstitialReady = true;
		// Handle the ad loaded event.
	}

	public static void HandleAdClosed (object sender, System.EventArgs args)
	{
		//print ("OOOO YEAHHHHH....... KAPANDIIIII event received.");
		// Handle the ad loaded event.
		interstitial.Destroy ();
		isInterstitialReady = false;
		createInstertialAd ();
	}

	public static void HandleAdLeftApplication (object sender, System.EventArgs args)
	{
		//print ("Reklam izlerken çıktı.");
		//googleAnalyticsScene.LogLeftApplication ("Reklam izlerken çıktı");
	}
	

	///<summary>
	///Random biçimde reklam gösterir. Göderilen int 1 ise reklam %100, 2 ise%50 çıkar
	///</summary>

	public static void showInterstitialAd (int _randomValue)
	{
		//if (SaveManager.GetAdsRemoved () == 0) {
			if (interstitial.IsLoaded ()) {
				int randomNumber = Random.Range (0, _randomValue);
				if (randomNumber == 0)
					interstitial.Show (); 
			} else{
			showInterstitialAd(1); 	//BU KOD TEST AMAÇLI HER BUTONA BASILDIĞINDA REKLAM
									//GÖSTERILMESI IÇIN YAZILDI...
			}
				//}
	}


	/*	
        public void showSmartBanner ()
        {
                if (SaveManager.appPurchased == 0) {
                        bannerView.LoadAd (adRequestObject);
                        bannerView.Show ();
                }
        }
         * */

	public static void hideSmartBanner ()
	{
		bannerView.Hide ();			
	}
	

	public static void showBanner ()
	{
		if (adRequestObject != null)
              //  print("YES NULL");
			bannerView.LoadAd (adRequestObject);
		bannerView.Show ();
            
	}

	public static void destroyBanner ()
	{
		bannerView.Destroy ();
	}


}




